#!/bin/bash
set -e

#modificamos el titulo de la pagina remplasando el texto 'SITENAME'	por el valor de la variavle de entorno '$SITENAME'
sed -i 's/SITENAME/'"$SITENAME"'/' /app/index.html


#Testear si dentro del direcctorio '/var/www/html' existe un fichero 'index.html'

if [ -f /var/www/html/index.html ]; then
	exit 0
else 
	cp /app/index.html /var/www/html
fi

exec "$@"

