#!/bin/bash

set -e

apt update && apt install wget -y

# descargamos la aplicacion

cd /usr/share/nginx/html && \
wget https://gitlab.com/christian.castagnola/docker-apps-example/-/raw/master/src/calculador.html \
-0 index.html

exec /entrypoint.sh "$@"

